require 'open-uri'
require 'nokogiri'

module CustomHelpers
  def icon(icon, cssclass = "", attrs = {})
    width = attrs[:width] || 76
    height = attrs[:height] || 76
    viewbox_width = attrs[:viewbox_width] || width
    viewbox_height = attrs[:viewbox_height] || height
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 #{viewbox_width} #{viewbox_height}", width: width, height: height, class: cssclass, aria: { label: label }, role: "img" do
      partial "includes/icons/#{icon}.svg"
    end
  end

  def svg_image(icon, cssclass = "", attrs = {})
    data = attrs[:data] || ""
    content_tag :svg, class: cssclass, role: "img", data: data do
      partial icon
    end
  end

  def xml_feed_content(article)
    content = article.body

    content << if article.data.image_title
                 "<img src='#{data.site.url}#{article.data.image_title}' class='webfeedsFeaturedVisual' style='display: none;' />"
               else
                 "<img src='#{data.site.url}#{image_path('default-blog-image.png')}' class='webfeedsFeaturedVisual' style='display: none;' />"
               end

    h(content)
  end

  def markdown(text)
    # Scope parameter is necessary to make Markdown in YAML work properly
    # See: https://github.com/middleman/middleman/issues/653#issuecomment-9954111
    Tilt['markdown'].new { text }.render(scope: self)
  end

  def kramdown(text)
    Kramdown::Document.new(text).to_html
  end

  def team_size
    data.team.count { |entry| entry['type'] == 'person' }
  end

  def open_roles
    data.roles.select(&:open).sort_by(&:title)
  end

  def salary_avail
    data.roles.select(&:salary).sort_by(&:title)
  end

  def current_role_for_salary_calculator
    current_role = salary_avail.detect do |role|
      role.description.start_with?("/#{File.dirname(current_page.request_path)}")
    end

    if current_role&.levels&.is_a? String
      current_role.levels = data.role_levels.send(current_role.levels)
    end

    current_role
  end

  def font_url(current_page)
    fonts = ["Source+Sans+Pro:200,300,400,500,600,700"]

    if current_page.data.extra_font
      fonts = fonts.concat current_page.data.extra_font
    end
    fonts = fonts.join("|")

    "//fonts.googleapis.com/css?family=#{fonts}"
  end

  def highlight_active_nav_link(link_text, url, options = {})
    options[:class] ||= ""
    options[:class] << " active" if url == current_page.url
    link_to(link_text, url, options)
  end

  def full_url(current_page)
    "#{data.site.url}#{current_page.url}"
  end

  def current_version
    ReleaseList.new.release_posts.first.version
  end

  def copy_btn_options(copy_text, tooltip_text = nil, button_class = nil)
    tooltip_text = 'Copy to clipboard' if tooltip_text.nil?
    button_class = 'copy-btn js-copy-btn' if button_class.nil?

    {
      class: "btn #{button_class}", type: 'button',
      title: tooltip_text, 'aria-label' => tooltip_text,
      data: {
        'clipboard-text' => copy_text,
        toggle: 'tooltip', placement: 'top'
      }
    }
  end

  def production?
    ENV['MIDDLEMAN_ENV'] == 'production'
  end
end
