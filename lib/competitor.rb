require 'yaml'

module Gitlab
  module Homepage
    class Competitor
      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      def gitlab?
        @key[0..6] == 'gitlab_'
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.all!
        @features ||= YAML.load_file('data/features.yml')
        @features['competitors'].map do |key, data|
          new(key, data)
        end
      end

      def self.for_stage(stage)
        all!.dup.keep_if do |competitor|
          !competitor.gitlab? && competitor.category && !(competitor.category & stage.categories.map(&:key)).empty?
        end
      end
    end
  end
end
