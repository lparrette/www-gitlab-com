---
layout: markdown_page
title: "Codefresh"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Codefresh is a CI/CD tool designed for containers and Kubernetes. Codefresh features a [GitLab integration](https://codefresh.io/docs/docs/integrations/git-providers/) that allows you to use GitLab for version control and run Codefresh pipelines on your code. Codefresh has some features that make it more mature than GitLab for running pipelines across multiple projects. But it lacks the benefits of a [single application](/handbook/product/single-application/).

Codefresh charges for builds per month, as well as concurrent builds. GitLab has no such limitations with the ability to elastically scale Runners to handle as many concurrent builds as needed on demand and then scale down so you aren't paying to keep inactive Runners up.

Codefresh only offers a self-managed option for Enterprise pricing. Free, Basic, and Pro tiers are for SaaS-only. GitLab offers self-managed and SaaS options at every price point.

## Resources
- [Codefresh homepage](https://codefresh.io/)
- [Comparison page on their site](https://codefresh.io/continuous-integration/codefresh-versus-gitlabci/)
- [Codefresh GitLab integration](https://codefresh.io/docs/docs/integrations/git-providers/)

## Comments/Anecdotes
- Codefresh makes some claims in their blog comparing themselves to GitLab that are not really accurate.
  - "GitlabCI isn’t designed for micro-services since everything is tied to a single project"
    - Although we can improve our microservices support, this claim is not true. GitLab has [multi project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) and can [trigger pipelines for mulit-projects via API](https://docs.gitlab.com/ee/ci/triggers/README.html#ci-job-token). In fact, The CI working group for CNCF chose GitLab to run their multi-project multi-cloud pipelines: [CNCF case study](/customers/cncf/), [CNCF video](https://www.youtube.com/watch?time_continue=372&v=Jc5EJVK7ZZk)
- We are missing some features identified in [this post](https://codefresh.io/continuous-integration/codefresh-versus-gitlabci/)) that would bring us on par with Codefresh
  - The ability to [define multiple pipelines](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972)
  - Support for monorepos with the ability to [run pipelines only on specific directories](https://gitlab.com/gitlab-org/gitlab-ce/issues/19232) - UPDATE: [Shipped in Gitlab 11.4](https://about.gitlab.com/2018/10/22/gitlab-11-4-released/#run-jobs-codeonlycodecodeexceptcode-for-modifications-on-a-path-or-file)
  - [Group level Docker registry browser](https://gitlab.com/gitlab-org/gitlab-ce/issues/49336)
  - [Group level Kubernetes clusters](https://gitlab.com/gitlab-org/gitlab-ce/issues/34758)
  - [Make container building first class](https://gitlab.com/gitlab-org/gitlab-ce/issues/48913)

## Pricing
- [Codefresh Pricing](https://codefresh.io/pricing/)
- Codefresh prices per build and per concurrent build
- $299 Pro tier is only 3 concurrent builds, to get more you have to call for pricing

## Comparison
