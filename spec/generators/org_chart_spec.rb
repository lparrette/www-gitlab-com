require 'tempfile'
require 'yaml'
require_relative '../../generators/org_chart'

describe OrgChart do
  TeamEntry = Struct.new(:name, :reports_to, :role, :slug, :speciality, :placeholder)

  let(:tmpfile) { Tempfile.new('test.yml.') }
  let(:team) do
    [
      TeamEntry.new('King Kong', nil, 'King', 'king', nil, false),
      TeamEntry.new('Donkey Kong', 'king', 'King', 'general', nil, false),
      TeamEntry.new('Q-Bert', 'general', 'Peon', 'q-bert', '@#%@#$', false),
      TeamEntry.new('PacMan', 'general', 'Peon', 'Pacman', 'eating ghosts', false),
      TeamEntry.new('Vacancy', 'king', 'vacancy', nil, true)
    ]
  end

  subject { OrgChart.new(tmpfile.path) }

  def stringify_keys(hash)
    {}.tap do |dest|
      hash.each do |key, value|
        dest[key.to_s] = value
      end
    end
  end

  before do
    data = team.map { |entry| stringify_keys(entry.to_h) }
    tmpfile.write(data.to_yaml)
    tmpfile.flush
  end

  after do
    FileUtils.rm(tmpfile.path)
  end

  describe '#validate!' do
    context 'ambiguous roles' do
      let(:team) do
        [
          TeamEntry.new('King Kong', nil, 'King', 'king', nil, false),
          TeamEntry.new('Donkey Kong', 'king', 'King', 'king', nil, false),
          TeamEntry.new('Q-Bert', 'king', 'Peon', 'Peon', '@#%@#$', false)
        ]
      end

      it 'detects amibiguous reporting structure' do
        expect { subject.validate! }.to raise_error(Exception)
      end
    end

    context 'reports_to references a role that nobody has' do
      let(:team) do
        [
          TeamEntry.new('King Kong', nil, 'King', 'king', nil, false),
          TeamEntry.new('Donkey Kong', 'general', 'King', 'king', nil, false)
        ]
      end

      it 'detects missing role' do
        expect { subject.validate! }.to raise_error(Exception)
      end
    end
  end

  describe '#team_data' do
    it 'augments team data with proper leads' do
      data = subject.team_data

      expect(data.count).to eq(team.count)

      data.each_with_index do |entry, index|
        expect(entry[:name]).to eq(team[index][:name])
        expect(entry[:speciality]).to eq(team[index][:speciality])
        expect(entry[:link]).to eq(team[index][:role])
        expect(entry[:placeholder]).to eq(team[index][:placeholder])
      end

      expected_leads = [
        nil,
        'King Kong',
        'Donkey Kong',
        'Donkey Kong',
        'King Kong'
      ]

      expect(data.map { |entry| entry[:lead]&.fetch('name') }).to eq(expected_leads)
    end
  end

  describe '#team_data_tree' do
    it 'builds children properly' do
      tree = subject.team_data_tree

      expect(tree.first[:name]).to eq('King Kong')
      expect(tree.first[:children].count).to eq(2)
      expect(tree.first[:children][0][:name]).to eq('Donkey Kong')
      expect(tree.first[:children][1][:name]).to eq('Vacancy')

      general_reports = tree.first[:children][0][:children]
      expect(general_reports.count).to eq(2)
      expect(general_reports[0][:name]).to eq('Q-Bert')
      expect(general_reports[1][:name]).to eq('PacMan')
    end
  end
end
